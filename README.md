
# Migracija MDC aplikacija
## Ažuriranje Node verzije
Za neke korake je potrebno ažurirati Node verziju, tako da je to najbolje da se uradi unapred. Najlakše preko nvm-a.
### Prvi korak
```bash
$ curl -o- [https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh
```
### Drugi korak
```bash
$ export NVM_DIR="$HOME/.nvm" 
$ [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
$ [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
```
### Treći korak
```bash
$ nvm use 14.15
```
#### Četvrti korak (opcionalno podešava default verziju)
```bash
$ nvm alias default 14.15
```
>U slučaju da koristite vscode terminal potrebno je restartovati vscode
## Ažuriranje globalne Angular verzije
```bash
npm uninstall -g @angular/cli
npm install -g @angular/cli@latest
```
## Ažuriranje globalne Ionic verzije
```bash
npm install -g ionic@5
```
## Angular 7.x -> 8.x
### Prvi korak
```bash
NG_DISABLE_VERSION_CHECK=1 npx @angular/cli@8 update @angular/cli@8 @angular/core@8
# DEPRECATED
# ng update @angular/cli@8 @angular/core@8
```
U slučaju da ovo prođe bez greške preći na sledeći korak (verovatno neće 😕).
Ovako izgleda greška koja će verovatno da se prikaže.
![Error](https://i.ibb.co/KspfL4H/Screenshot-from-2021-09-20-13-33-23.png)
Potrebno je da se odradi sledeće za @angular/core i @angular/common. 
```bash
npm i @angular/core@8.2.14
npm i @angular/common@8.2.14
```
Ako za <em>**ng update**</em> ponovo izađe ista greška, ažurirati i te pakete.
### Drugi korak
Od Angular verzije 8 potrebno je da ViewChild komponente sadrže atribut static, što znači da svaka upotreba ViewChild komponente mora da se izmeni tako što se doda static atribut na true ili false u zavisnosti od slučaja.
```js
❌ @ViewChild('element') element;
✅ @ViewChild('element', { static: true }) element;
```
Ovo može da se automatizuje sledećom naredbom
```bash
 ng update @angular/core --from 7 --to 8 --migrate-only --force --allow-dirty
```
## Ionic 4.x -> 5.x

### Prvi korak
```bash
npm install @ionic/angular@5 --save
```
### Drugi korak
Iz Ionic verzije 5 izbačen je <em>**Events**</em> servis, što znači da moramo da ga zamenimo odgovarajućim Rxjs Subject-om. Ovo se radi tako što se izbaci svaka upotreba Events servisa.
Events implementira publish subscribe obrazac tako da se lako vrši konverzija u Subject oblik.
```js
❌
this.events.publish('type:message', payload);
this.events.subscribe('type:message', (payload) => {...});
✅
// Servis
private message$ = new Subject<type>();
public publishMessage(payload: type) { this.message$.next(payload)); }
public getMessageObservable() { return this.message$.asObservable(); }

// Upotreba analogna upotrebi events servisa
this.typeService.publishMessage(payload);
this.typeService.getMessageObservable().subscribe((payload) => {...});
```
Subject treba ubaciti u servis u koji ima najviše smisla da stoji, to je najcesce ono sto se nalazi u <em>**type**</em> delu naziva eventa. Dok se naziv subjecta može nazvati po <em>**message**</em> delu.
### Treći korak
Komponenta <em>**ion-select-option**</em> nema više selected atribut, tako da mora da se setuje default vrednost u ion-select komponenti umesto u ion-select-option.
```html
❌
<ion-select>
	<ion-select-option *ngFor="let  item  of  items"
	[selected]="isSelected(item)" [value]="item">
		{{item}}
	</ion-select-option>
</ion-select>

✅
<ion-select [value]="getDefaultItem()">
	<ion-select-option *ngFor="let item of items" [value]="item">
		{{item}}
	</ion-select-option>
</ion-select>
``` 
### Četvrti korak
Ako se u projektu koristi <em>**ToastController**</em> potrebno je izmeniti argumente koji se prosleđuju prilikom prezentacije toast-a.
```
❌
this.toastController.create({
	showCloseButton: true,
	closeButtonText: 'Test',
	...
});
✅
this.toastController.create({
buttons: [{
	text: 'Test',
	role: 'cancel',
	...
	}]
});
```
### Peti korak
Potrebno je prebaciti <em>**(tap)**</em> evente u <em>**(click)**</em>.
```html
❌
<ion-col (tap)="fn()">Btn</ion-col>
✅
<ion-col (click)="fn()">Btn</ion-col>
```
## Angular 8.x -> 9.x
### Prvi korak
```bash
ng update @angular/cli@9 @angular/core@9
```
U slučaju da ovo prođe bez greške preći na sledeći korak. 
Ovako izgleda greška koja će verovatno da se prikaže.
![Error](https://i.ibb.co/Y0m7g6Q/Screenshot-from-2021-09-20-15-24-03.png)
Potrebno je da se odradi sledeće za @angular/core i @angular/common. 
```bash
npm i @angular/core@9.1.13
npm i @angular/common@9.1.13
npm i @angular-devkit/core@9.1.15
npm i @angular-devkit/schematics@9.1.15
```
Ako za <em>**ng update**</em> ponovo izađe ista greška, ažurirati i te pakete.
### Drugi korak
Ažuriranje verzije typescripta
```bash
npm install typescript@3.8.3
```
### Treći korak
Izmeniti sve <em>**ion-button**</em> komponente button komponentom sa ion-button stilom.
```html
❌
<ion-button>Button</ion-button>
✅
<button ion-button>Button</button>
```
### Četvrti korak
Iz svih modula izbacity <em>**entryComponents**</em>, jer nisu više potrebni od verzije 9.
### Peti korak (probati update paketa?)
Iz projekta izbaciti <em>**ng-recaptcha**</em> paket.
```js
❌
import { RecaptchaModule } from  'ng-recaptcha';
import { RecaptchaFormsModule } from  'ng-recaptcha/forms';
...
@NgModule({
	imports: [
	...
	RecaptchaModule.forRoot(),
	RecaptchaFormsModule
]
```
### Šesti korak
U fajl <em>**main.ts**</em> dodati sledeći import na početku.
```js
import  '@angular/compiler';
```
## Priprema za build Angular 9.x
Potrebno je adaptirati <em>**@angular-devkit/\***</em> verzije na semantički iste (vršnjačke) <em>**@angular/cli**</em> verzije (posetiti npmjs.com da se pronađu odgovarajuće verzije)
```js
// package.json za @angular/cli 9.1.15
"@angular-devkit/architect": "0.901.15",
"@angular-devkit/build-angular": "0.901.15",
"@angular-devkit/core": "^9.1.15",
"@angular-devkit/schematics": "^9.1.15",
```
Nakon toga verziju <em>**@angular/angular-toolkit**</em> treba podići na 2.3.3.
```js
"@ionic/angular-toolkit": "~2.3.3"
```
## Angular 9.x -> 10.x
### Prvi korak
```bash
ng update @angular/core@10 @angular/cli@10
```
U slučaju da ovo prođe bez greške preći na sledeći korak. 
Ovako izgleda greška koja će verovatno da se prikaže.
![Error](https://i.ibb.co/9Gkc4mT/Screenshot-from-2021-09-21-14-59-26.png)
Potrebno je da se odradi sledeće za @angular/core i @angular/common. 
```bash
npm i @angular/core@10.2.5
npm i @angular/common@10.2.5
```
Ako za <em>**ng update**</em> ponovo izađe ista greška, ažurirati i te pakete.
### Drugi korak
Putanje resursa u scss fajlovima moraju da budu relativne u odnosu na aktuelni scss fajl.
```
src/
├─ themes/
│  ├─ styles/
│  │  ├─ images.scss
├─ assets/
│  ├─ svg/
│  │  ├─ logo.svg
```
```scss
❌
background-image: url('assets/svg/logo.svg');
✅
background-image: url('../../assets/svg/logo.svg');
```
## Angular 10.x -> 11.x
### Prvi korak
```bash
ng update @angular/core@11 @angular/cli@11
```
U slučaju da ovo prođe bez greške preći na sledeći korak. 
Ovako izgleda greška koja će verovatno da se prikaže.
![Error](https://i.ibb.co/MBK0PRN/Screenshot-from-2021-09-21-15-56-07.png)
Potrebno je da se odradi sledeće za @angular/core i @angular/common. 
```bash
npm i @angular/core@11.2.14
npm i @angular/common@11.2.14
```
Ako za <em>**ng update**</em> ponovo izađe ista greška, ažurirati i te pakete.
### Drugi korak
Iz <em>**package.json**</em> izbaciti sledeće pakete:
 - "@types/jasmine"
 - "@types/jasminewd2"
 
 I instalirati sledeći paket:
```bash
npm i @types/jasminewd2@latest
```
## Angular 11.x -> 12.x
### Prvi korak
```bash
ng update @angular/core@12 @angular/cli@12
```
U slučaju da ovo prođe bez greške preći na sledeći korak. 
Ovako izgleda greška koja će verovatno da se prikaže.
![Error](https://i.ibb.co/mcFCPqL/Screenshot-from-2021-09-21-16-31-47.png)
Potrebno je da se odradi sledeće za @angular/core i @angular/common. 
```bash
npm i @angular/core@12.2.6
npm i @angular/common@12.2.6
```
Ako za <em>**ng update**</em> ponovo izađe ista greška, ažurirati i te pakete.
## Priprema za build
U <em>**version.ts**</em> fajlu treba izmeniti verziju u neku veću cifru kako ne bi iskakao prozor koji primorava korisnika da ažurira aplikaciju.
```js
// src/app/_config/version.ts
❌
export const VERSION = '1.0.0';
✅
export const VERSION = '25.0.0';
```
